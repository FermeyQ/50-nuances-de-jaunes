<?php

class Query extends PDO
{
    private $dsn = "mysql:dbname=50nuancesdejaune;host=localhost;charset=utf8";
    private $user = "root";
    private $password = " ";
    private $db;

    public function __construct()
    {
        try {
            $this->db = new PDO($this->dsn, $this->user, $this->password);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        }

        catch (PDOException $e) {
            Log::logWrite($e->getMessage());
        }
    }

    public function insert($requete)
    {

    }

    public function __destruct()
    {
        unset($this->db);
    }
}
